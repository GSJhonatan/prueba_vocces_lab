<?php

namespace App\Http\Controllers\Api\Company;

use App\Http\Controllers\Controller;
use Vocces\Company\Application\CompanyList;

class GetAllCompanyController extends Controller
{
 
    /**
     * List all company
     *
     */
    public function __invoke( CompanyList $service)
    {
        try {
            $company = $service->handle();
            return response($company, 200);
        } catch (\Throwable $error) {
           
            throw $error;
        }
    }
}
