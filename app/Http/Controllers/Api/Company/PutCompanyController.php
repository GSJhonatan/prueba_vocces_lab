<?php

namespace App\Http\Controllers\Api\Company;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests\Company\UpdateCompanyRequest;
use Vocces\Company\Application\CompanyUpdated;

class PutCompanyController extends Controller
{
    /**
     * Update a company
     *
     * @param \App\Http\Requests\Company\UpdateCompanyRequest $request
     * @param string $id
     * @param  \Vocces\Company\Application\CompanyUpdated $service
     */
    public function __invoke(UpdateCompanyRequest $request,$id, CompanyUpdated $service)
    {
        DB::beginTransaction();
        try {
            $request = $request->toArray();
            $company = $service->handle($id);
            DB::commit();
            return response($company, 202);
        } catch (\Throwable $error) {
            DB::rollback(); 
            throw $error;
        }
    }
}
