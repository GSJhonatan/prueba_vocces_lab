<?php

namespace Tests\Vocces\Company\Infrastructure;

use Vocces\Company\Domain\Company;
use Vocces\Company\Domain\CompanyRepositoryInterface;

class CompanyRepositoryFake implements CompanyRepositoryInterface
{
    public bool $callMethodCreate = false;
    public bool $callMethodUpdate = false;
    public bool $callMethodList = false;

    /**
     * @inheritdoc
     */
    public function create(Company $company): void
    {
        $this->callMethodCreate = true;
    }

    /**
     * Update a company instance
     *
     * @inheritdoc
     */
    public function update($id): bool
    {
        $this->callMethodUpdate = true;
    }
    
    /**
     * @inheritdoc
     */
    public function list(): object
    {
        $this->callMethodList = true;

    }
}
