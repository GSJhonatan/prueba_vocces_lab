<?php

namespace Tests\Vocces\Company\Routes;

use Tests\TestCase;

class ListAllCompanyRouteTest extends TestCase
{
    
    /**
     * @group route
     * @group access-interface
     * @test
     */
    public function listAllCompanyRoute()
    {

        /**
         * Actions
         */
        $response = $this->json('GET', '/api/company-list');

        /**
         * Asserts
         */
        $response->assertStatus(200);
    }
}
