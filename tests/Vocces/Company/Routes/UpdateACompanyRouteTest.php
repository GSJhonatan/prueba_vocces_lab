<?php

namespace Tests\Vocces\Company\Routes;

use Tests\TestCase;

class UpdateACompanyRouteTest extends TestCase
{
    /**
     * @group route
     * @group access-interface
     * @test
     */
    public function updateACompanyRoute()
    {
        
        /**
         * Actions
         */
        $response = $this->json('PUT', '/api/company-update/90c356f1-f099-4f65-b000-7f21fc5ada81');

        /**
         * Asserts
         */
        $response->assertStatus(202);
    }
}
