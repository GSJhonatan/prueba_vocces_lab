<?php

namespace Vocces\Company\Infrastructure;

use App\Models\Company as ModelsCompany;
use Vocces\Company\Domain\Company;
use Vocces\Company\Domain\CompanyRepositoryInterface;
use Vocces\Company\Domain\ValueObject\CompanyStatus;

class CompanyRepositoryEloquent implements CompanyRepositoryInterface
{
    /**
     * @inheritDoc
     */
    public function create(Company $company): void
    {
        ModelsCompany::Create([
            'id'     => $company->id(),
            'name'   => $company->name(),
            'email'  => $company->email(),
            'address'=> $company->address(),
            'status' => $company->status()
        ]);
    }
    /**
     * @inheritDoc
     */
    public function list(): object
    {
       return ModelsCompany::get();
    }

    /**
     * @inheritDoc
     */
    public function update($id): bool
    {
        $company = ModelsCompany::findOrFail($id);

        return $company->update(['status'=> CompanyStatus::enabled()]);
    }
}
