<?php

namespace Vocces\Company\Domain;

interface CompanyRepositoryInterface
{
    /**
     * Persist a new company instance
     *
     * @param Company $company
     *
     * @return void
     */
    public function create(Company $company): void;
   
    /**
     * Update a company instance
     *
     * @param $id
     *
     * @return bool
     */
    public function update($id): bool;

    /**
     * List all company instance
     *
     *
     * @return collection
     */
    public function list(): object;
}
