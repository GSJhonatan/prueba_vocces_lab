<?php

use App\Http\Controllers\Api\Company\GetAllCompanyController;
use App\Http\Controllers\Api\Company\PostCreateCompanyController;
use App\Http\Controllers\Api\Company\PutCompanyController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/company', [PostCreateCompanyController::class, '__invoke']);
Route::get('/company-list', [GetAllCompanyController::class, '__invoke']);
Route::put('/company-update/{id}', [PutCompanyController::class, '__invoke']);
